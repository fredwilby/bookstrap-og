'use strict';

var path = require('path');
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');

var cfg = require('./server/config');

var app = express();

var isDev = process.env.NODE_ENV === 'development';

if(isDev)
{
	require('./server/dev')(app);
}

app.use(cookieParser());
app.use(bodyParser.urlencoded({	extended: false }));
app.use(bodyParser.json());

app.use(session({
	secret: cfg.secret,
	resave: true,
	saveUninitialized: true
}));

require('./server/passport.config.js')(passport);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

mongoose.connect(cfg.DB_URL);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('Mongo awakens');
});

var authRoutes = require('./server/routes/auth')(express, passport);
var apiRoutes = require('./server/routes/api')(express);

/* Required for openshift monitoring */
app.use('/health', function(req, res) {
	res.status(200).send();
});

app.use('/static', express.static('static'));
app.use('/auth', authRoutes);
app.use('/api', apiRoutes);

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});


app.listen(cfg.PORT, cfg.HOST, function(err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at '+cfg.HOST+':'+cfg.PORT);
});
