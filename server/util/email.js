var config = require('../config');
var sendgrid = require('sendgrid')(config.sendgridAPIKey)

module.exports = function(email, token) {
  var em = new sendgrid.Email();
  em.addTo(email);
  em.setFrom('no-reply@bookstrap.xyz');
  em.setSubject('Welcome to Bookstrap!');

  var getValUrl = function() {
    return 'http://localhost:8002/auth/validate/'+encodeURIComponent(email)+'/'+token+'/';
  };

  em.setHtml('<p style="font-size:1.25em; font-family:sans-serif;">You\'re almost ready to start partying at bookstrap!, just go to <a href="'+getValUrl()+'"/>this link!</a></p><p>If that doesn\'t work, try pasting this url into your browser: '+getValUrl()+'</p>');
  sendgrid.send(em, function(err, json) {
    if(err) console.log(err);
    if(json) console.log(json);
  });
}
