
module.exports = function(express) {

  var router = express.Router();
  var api = require('../controllers/api');

  /* public routes */
  router.route('/books').get(api.books.get);
  router.route('/books/:id').get(api.books.getById);
  router.route('/users/:id').get(api.users.getById);

  router.route('/books/lookup/:asin').get(api.books.lookup);
  router.route('/books/search/:title').get(api.books.search);

  /* private routes */
  router.use(function(req, res, next) {
    if(req.isAuthenticated())
      return next();

    return res.status(401).json({'error': 'Please log in to access this page'});
  });

  router.route('/books/recommend').post(api.books.recommend);
  router.route('/books/tag').post(api.books.tag);
  router.route('/tags/add').post(api.tags.add);

  router.route('/me').post(api.users.profile);

  return router;
}
