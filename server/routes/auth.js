module.exports = function(express, passport) {

  var authRouter = express.Router();
  var auth = require('../controllers/auth');

  authRouter
    .route('/login')
    .post(passport.authenticate('local-login'), auth.login);

  authRouter.route('/signup').post(auth.signup);

  authRouter.route('/validate/:email/:token').get(auth.validate);

  authRouter.route('/facebook').get(passport.authenticate('facebook', { scope: ['email', 'public_profile'] }));
  authRouter.route('/callback').get(passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/'
  }));

  authRouter.use(function(req, res, next) {
    if(req.isAuthenticated())
      return next();

    return res.status(401).json({'error': 'Please log in to access this page'});
  });

  authRouter.route('/me').get(auth.me);

  authRouter.route('/logout').get(auth.logout);


  return authRouter;
}
