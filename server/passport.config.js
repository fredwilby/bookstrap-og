var User = require('./models/User');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var config = require('./config');

module.exports = function(passport) {
	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		return done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		//this may throw error because you are passing a callback
		User.findOne({_id:id}, function(err, user) {
			if (err) return done(err);
			return done(null, user);
		});
	});

	// =========================================================================
	// LOCAL LOGIN =============================================================
	// =========================================================================
	passport.use('local-login', new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password'
	},
	function(email, password, done) {
		if (email) email = email.toLowerCase();
		// asynchronous

		process.nextTick(function() {
			User.findOne({ username: email })
			.select('password valid')
			.exec(function(err, user) {
				if (err) return done(err.message);
				if (!user) return done(null, false);
				if (!user.valid) return done('Email address needs to be confirmed');
				if (!user.validPassword(password)) return done(null, false);
				return done(null, user);
			});
		});
	}));

	// Facebook login

	passport.use('facebook', new FacebookStrategy(config.facebook,
		function(access_token, refresh_token, profile, done) {
			process.nextTick(function() {

				User.findOne({ 'facebookId': profile.id }, function(err, user) {
					if(err) return done(err);

					if(user && user.valid) return done(null, user);

					var email = '', first_name = '', last_name = '', profilePic = '';

					if(profile.emails)
						email = profile.emails[0];

					if(profile.name.givenName)
						first_name = profile.name.givenName;

					if(profile.name.familyName)
						last_name = profile.name.familyName;

					if(!first_name && !last_name && profile.displayName) {
						if(profile.displayName.indexOf(' ') !== -1) {
							first_name = profile.displayName.split(' ')[0];
							last_name = profile.displayName.split(' ').slice(1).join(' ');
						} else {
							first_name = profile.displayName;
						}
					}

					if(profile.picture) {
						profilePic = profile.picture;
					} else {
						profilePic = '/static/images/profile.png';
					}

					User.create({
						email: email,
						first_name: first_name,
						last_name: last_name,
						facebookId: profile.id,
						profile_pic: profilePic,
						valid: true
					}, function(err, user) {
						if(err) return done(err);

						return done(null, user);
					});

				});

			});
		}
	));

};
