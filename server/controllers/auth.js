var User = require('../models/User');
var sendEmail = require('../util/email');

module.exports.login = function(req, res) {
  if(!req.user) return res.status(401).end();

  return res.status(200).end();
};

module.exports.me = function (req, res) {
	return res.json(req.user);
};

module.exports.logout = function (req, res) {
	req.logout();
	return res.status(200).end();
};

module.exports.signup = function(req, res) {

	var email = req.body.email;
  var password = req.body.password;

  if(!email || !password) {
    res.status(401).end();
  }

	User.findOne({'username': email}, function(err, user) {
		if (err) return res.status(500).json(err).end();
    if(user) return res.status(400).end();

    User.create({ username: email, email: email, password: password, valid: false }, function(err, user) {
      if(err) return res.status(500).json({'error':err.message}).end();

      sendEmail(user.email, user.token);

      return res.status(200).end();
    });


	});
};

module.exports.validate = function(req, res) {

  if(!req.params.token || !req.params.email)
    return res.status(400).send();

  User.findOne({ username: req.params.email, token: req.params.token }, function(err, user) {
    if(err) res.status(500).json(err);
    if(!user) res.status(404).send();

    user.valid = true;
    user.token = undefined;

    user.save(function(err) {
      if(err) res.status(500).json(err);

      req.login(user, function(err) {
        if(err) res.status(500).json(err);

        return res.redirect('/');
      });

    });
  });
};
