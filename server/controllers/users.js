var User = require('../models/User');

var getById = function(req, res) {
  if(!req.params.id)  return res.status(400).send();

  User.findOne({ _id: req.params.id }, function(err, user){
    if(err) return res.status(500).send();
    if(!user) return res.status(404).send();

    return res.status(200).json(user);
  });
}

var updateProfile = function(req, res) {

  res.status(500).json({ error: 'not implemented'});

}

module.exports = {
  getById: getById,
  profile: updateProfile
};
