var Book = require('../models/book');

var aws = require('aws-lib');

var config = require('../config');

var productApi = aws.createProdAdvClient(config.AWSAccessKeyId, config.AWSSecretKey, config.AssociateTag);

var getBooks = function(req, res) {

  Book.find({}, function(err, books) {
    if(err) return res.status(500).json(err);

    res.status(200).json(books);
  });
};

var getBookById = function(req, res) {
  Book.findOne({_id: req.params.id}, function(err, book) {
    if(err) res.status(500).json(err);
    if(!book) res.status(404).send();

    res.status(200).json(book);
  });
};

var addBook = function(req, res) {

  if(!req.params.asin)
    res.status(400).json({ error: 'no amazon url sent'});

  var asin = req.params.asin;

  if(!asin || asin.length < 2)
    res.status(400).json({  error: 'couldn\'t find asin in url' });

  productApi.call('ItemLookup',
    { ItemId: asin, IdType:'ASIN', ResponseGroup:'Images,ItemAttributes,EditorialReview'},
    function(err, result) {
      if(err) res.status(500).json(err);

      var item = result.Items.Item;

      var attrs = item.ItemAttributes;

      var title = attrs.Title;
      var headline = '';
      var summary = '';

      if(title.indexOf(':') !== -1) {
        headline = title.split(':').slice(1).join(':').trim();
        title = title.split(':')[0];
      }

      if(item.EditorialReviews && item.EditorialReviews.EditorialReview) {
        summary = item.EditorialReviews.EditorialReview.Content;
      }

      Book.findOne({ asin: item.ASIN }, function(err, book) {
        if(err) res.status(500).json(err);

        if(!book) {
          Book.create({
            author: attrs.Author,
            title: title,
            headline: headline,
            image: item.LargeImage.URL,
            asin: item.ASIN,
            summary: summary,
            url: 'http://www.amazon.com/dp/'+item.ASIN+'/?tag='+config.AssociateTag

          }, function(err, book) {
            if(err) res.status(500).json(err);

            return res.status(200).json(book);
          });
        } else {
          res.status(200).json(book);
        }

      });
    });
}

var amazonSearch = function(req, res) {

  if(!req.params.title || typeof req.params.title !== 'string')
    res.status(400).send();

  productApi.call('ItemSearch',
    { Keywords: req.params.title, SearchIndex: 'Books', ResponseGroup:'Images,ItemAttributes' },
    function(err, result) {
      if(err) res.status(500).json(err);

      res.status(200).json(result);
    });
}


var notImplemented = function(req, res) {
  return res.status(500).json({'error': 'not implemented'});
}

module.exports = {
  get: getBooks,
  getById: getBookById,
  lookup: addBook,
  search: amazonSearch,
  recommend: notImplemented,
  tag: notImplemented
};
