
module.exports = {
  books: require('./books'),
  tags: require('./tags'),
  users: require('./users')
};
