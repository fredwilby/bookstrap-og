
var Tag = require('../models/Tag');

var addTag = function(req, res, next) {

  if(!req.body.tag)
    res.status(400).send();

  Tag.create({ tag: req.body.tag }, function(err, tag) {
    if(err) return res.status(500).json({ error: err.message });

    return res.status(200).json(tag);
  });
};

module.exports = {
  add: addTag
};
