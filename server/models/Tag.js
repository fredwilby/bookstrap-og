var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Tag = new Schema({

  tag: { type: String, required: true }

});

 module.exports = mongoose.model('Tag', Tag);
