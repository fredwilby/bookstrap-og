var mongoose = require('mongoose'),
    autopopulate = require('mongoose-autopopulate'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    crypto = require('crypto');

var User = new Schema({
  username: { type: String },
  email: { type: String },
  first_name: {type: String },
  last_name: {type: String },
  password: { type: String, select: false },
  token: {type: String, default: crypto.randomBytes(17).toString('hex'), select: false},
  valid: { type: Boolean, default: false },

  profile_pic: { type: String, default: '/static/images/profile.png' },

  facebookId: { type: String },

  liked: [{ type: Schema.ObjectId, ref:'Book', autopopulate: true }],

}, { timestamps: true });

User.pre('save', function(next) {

  if(!this.isModified('password')) return next();

  if(this.password.length < 8 || this.password.length > 100) {
    return next(new Error('Password must be between 8 and 100 characters'))
  }

  console.log('trying to save');
  var self = this;

  bcrypt.hash(this.password, null, null, function(err, hash) {
    if(err) return next(err);

    self.password = hash;
    next();
  });
});


User.methods.validPassword = function(pass)  {
  var user = this;
  return bcrypt.compareSync(pass, user.password);
}

User.plugin(autopopulate);

module.exports = mongoose.model('User', User);
