var mongoose = require('mongoose');
var autopopulate = require('mongoose-autopopulate');

var Schema = mongoose.Schema;

var bookSchema = new Schema({
	url:String,
	recdBy:[{ type: Schema.ObjectId, ref: 'User', autopopulate: true }],
	summary:String,
	title:String,
	headline: String,
	author:String,
	category:String,
	votes: Number,
	asin: String,
	image:String,
	tags: [{ type: Schema.ObjectId, ref: 'Tag', autopopulate: true }],
}, { timestamps:true });

bookSchema.index({ votes:-1, createdAt:-1 });
bookSchema.plugin(autopopulate);

module.exports = mongoose.model('Book', bookSchema);
