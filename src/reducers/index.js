import { combineReducers } from 'redux-immutable';

import bookReducer from './bookReducer';
import userReducer from './authReducer';

export default combineReducers({
  books: bookReducer,
  user: userReducer
});
