import { bookActions } from '../actions/bookActions';
import { fromJS } from 'immutable';

import { pending, fulfilled, rejected } from '../actionStates';

const initialState = fromJS({

  loading: false,
  fetched: false,
  list: {},

  error: null,
  amazonLoading: null,
  amazonResult: null
});

 export default (state = initialState, action) => {

   switch (action.type) {
     case pending(bookActions.FETCH_BOOKS):
        return state.set('loading', true);

     case fulfilled(bookActions.FETCH_BOOKS):

        let books = {};
        for(let book of action.payload.data) {
          books[book._id] = book;
        }

        return state.set('list', state.get('list').merge(books))
                  .set('loading', false).set('fetched', true);

    case rejected(bookActions.FETCH_BOOKS):
      return state.set('error', action.payload).set('loading', false);

    case pending(bookActions.ADD_BOOK):
    case pending(bookActions.SEARCH):
      return state.set('amazonLoading', true);

    case fulfilled(bookActions.SEARCH):
      return state.set('amazonLoading', false).set('amazonResult', action.payload.data);

    case fulfilled(bookActions.ADD_BOOK):
      let book = {};
      book[action.payload.data._id] = action.payload.data;

      return state.set('amazonLoading', false).set('amazonResult', action.payload.data).set('list', state.get('list').merge(book));

    case bookActions.CLEAR_RESULT:
      return state.set('amazonResult', null);

   }

   return state;
 }
