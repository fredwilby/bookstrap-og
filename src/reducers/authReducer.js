import { pending, fulfilled, rejected } from '../actionStates';
import { fromJS } from 'immutable';

import { userActions } from '../actions/userActions';

const initialState = fromJS({
  has_fetched: false,
  user: null
});

export default (state = initialState, action) => {

  switch (action.type) {

    case pending(userActions.GET_USER):
      return state.set('loading', true);

    case fulfilled(userActions.GET_USER):
      return state.set('loading', false).set('has_fetched', true).set('user', fromJS(action.payload.data));

    case rejected(userActions.GET_USER):
      return state.set('loading', false).set('has_fetched', true).set('user', null);

    case fulfilled(userActions.LOGIN):
      return state.set('has_fetched', false);


  }

  return state;
};
