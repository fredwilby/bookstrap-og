import React from 'react';
import { render } from 'react-dom';

import { Router, Route, IndexRoute, IndexRedirect } from 'react-router';
import { createHistory } from 'history';

import App from './App';

import { Provider } from 'react-redux';
import store from './store';

import Explore from './pages/Explore';
import LoginPage from './pages/LoginPage';

render(<Provider store={store}><Router history={createHistory()} >
	<Route path="/" component={App} >
		<IndexRoute component={Explore} />
		<Route path='/login' component={LoginPage} />
	</Route>
	</Router></Provider>, document.querySelector('.react-container'));
