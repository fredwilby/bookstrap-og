import React, { Component } from 'react';

import { connect } from 'react-redux';

import styles from '../styles/LoginPage.css';

class LoginPage extends Component {
  render() {
    return (<div className={styles.login}>
      <h2 className={styles.title}>Sign in/Sign up</h2>
      <h3 className={styles.tagline}>Share and discover the best startup books</h3>
      <img src="/static/images/mountains.svg" className={styles.icon} />
      <p className={styles.smallText}>See what your friends are raving about</p>
      <a href='/auth/facebook' className={styles.button}>Facebook</a>
      <p className={styles.smallText}>We'll never post to Twitter or Facebook without your permission</p>
      </div>);
  }
}

const stateMap = (state, props) => ({

});

export default LoginPage;
