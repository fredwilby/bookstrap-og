import React, {Component} from 'react';

import Header from '../components/Header/';
import BookTile from '../components/BookTile/';
import BookDetails from '../components/BookDetails/';
import AddBook from '../components/AddBook';

import { getUser } from '../actions/userActions';
import { fetchBooks, lookupBook, clearResult } from '../actions/bookActions';
import { connect } from 'react-redux';

class Explore extends Component {

  constructor(props) {
    super(props);

    this.state = { book: null, addOpen: false };
  }

  componentDidMount() {
    this.props.fetchBooks();

    if(!this.props.has_fetched && !this.props.user_loading)
      this.props.getUser();
  }

  componentWillReceiveProps(props) {
    if(!props.has_fetched && !props.user_loading)
      this.props.getUser();
  }

  lookup = () => {
    this.props.lookupBook('1608867951');
  }

  bookClick(id) {
    this.setState({ book: id });
  }

  closeModal = () => {
    this.setState({ book: null });
  }

  closeAddModal = (id) => {
    let stat = { addOpen: false };

    if(id) {
      stat.book = id;
      this.props.clearResult();
    }

    this.setState(stat);
  }

  openAdd = () => {
    if(this.props.user) {
      this.setState({ addOpen: true });
    } else {
      this.props.history.push('/login');
    }
  }

  render() {

    let profilePic = '/static/images/profile.png';

    if(this.props.user) {
      profilePic = this.props.user.get('profile_pic');
    }

    let openBook = null;
    if(this.props.books && this.state.book) {
      openBook = this.props.books.get(this.state.book);
    }

    let books = this.props.books.sort((a,b)=>
    (new Date(b.get('updatedAt')).getTime() - new Date(a.get('updatedAt')).getTime()));

    return (<div><Header profilePic={profilePic} onAddClick={this.openAdd} />
      {

         books.map((el, ind)=>(
           <BookTile title={el.get('title')}
            image={el.get('image')}
            headline={el.get('headline')}
            author={el.get('author')}
            votes={el.get('votes')}
            key={ind}
            onClick={this.bookClick.bind(this, el.get('_id'))}
            />)).toArray()
      }
    <BookDetails book={openBook} onClose={this.closeModal} />
    <AddBook open={this.state.addOpen} onClose={this.closeAddModal} />
    </div>);
  }

}


export default connect(
  (state, props) => ({
    loading: state.get('books').get('loading'),
    books: state.get('books').get('list'),
    has_fetched: state.get('user').get('has_fetched'),
    user_loading: state.get('user').get('loading'),
    user: state.get('user').get('user')
  }),
  {
    fetchBooks,
    lookupBook,
    getUser,
    clearResult
  }

)(Explore);
