import React, {Component} from 'react';
import { findDOMNode } from 'react-dom';

import Timeline from '../components/Timeline';
import WordTyper from '../components/WordTyper';

import axios from 'axios';
import styles from '../styles/LandingPage.css';

import { Link } from 'react-router';

import { Logo } from '../components/Util';

const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

const words = ['bootstrapping', 'growth', 'customer development', 'getting funded', 'success', 'your startup'];

export default class LandingPage extends Component {

  constructor(props) {
    super(props);
    this.state = { dialog: false, success:false };
  }

  displayDialog = () => {
    this.setState({ dialog: true });
  };

  closeDialog = () => {
    this.setState({ dialog: false});
  };

  submitEmail = () => {
    let email = findDOMNode(this.refs.email).value.trim();

    if(email.length > 0  && emailRegex.test(email))
    {
      findDOMNode(this.refs.resp).innerText = "";

      axios.post('/email', { email:email }).then((resp)=>{

        if(resp && resp.data === 'success')
          this.setState({success:true});
        else
          findDOMNode(this.refs.resp).innerText = "Email invalid! Please enter a valid email and retry.";

      }).catch((resp)=> {
        findDOMNode(this.refs.resp).innerText = "Email invalid! Please enter a valid email and retry."
      });
    }


  }

  render() {

    let dialogClass = styles.dialogSmall, dialogBack = styles.dialogBack;

    if(!this.state.dialog)
    {
      dialogClass += " " + styles.hidden;
      dialogBack += " " + styles.hidden;
    }

    let dialogContent = <div><h1 className={styles.popupTitle}>It won't be long</h1>
    <h2 className={styles.popupTagline}>We're about to go to the presses</h2>
    <img className={styles.mountains} src="/static/images/mountains.svg" />
    <p className={styles.reserve}>Reserve your spot</p>
    <div className={styles.inputBox}>
    <input className={styles.input} type="email" ref="email" placeholder="jsmith@example.com" /><a onClick={this.submitEmail} className={styles.button} ><img src="static/arrow.svg" className={styles.arrow} /></a></div>
      <p className={styles.emailStatus} ref="resp">We are serious about privacy and will NOT spam you</p></div>;

    if(this.state.success)
    {
      dialogContent = <div><h1 className={styles.popupTitle}>You ROCK!</h1><br/>
      <img className={styles.mountains} src="/static/images/rocktagon.png" />
      <p className={styles.reserve}>We'll make sure you get the press release!</p>
      </div>
    }

    return (<div className={styles.container}><div className={styles.containerInner}>
      <Logo />

      <div className={styles.heroBox}>
        <h1 className={styles.hero}>Crowd curated startup books</h1>
        <h1 className={styles.tagline}>Find the right book for <WordTyper className={styles.underline} words={words} /></h1>

        <Link to="/trending" className={styles.signupButton} >Strap in</Link>
        <p className={styles.belowButton}>Always free, no signup required :)</p>
      </div>

      <div className={styles.bottomBox}>
        <img className={styles.leanLeft} src="/static/images/startwithwhy.jpg" />
        <img className={styles.leanRight} src="/static/images/howtonetwork.jpg" />
        <img className={styles.straightUp} src="/static/images/leanstartup.jpg" />
      </div>

      <div className={dialogBack} onClick={this.closeDialog} ></div>
      <div className={dialogClass} >
        <a className={styles.closeDialog} title="I don't want cool books" onClick={this.closeDialog}>&#x2A2F;</a>
        {/*}
        <h1 className={styles.popupTitle}>What stage is your startup in?</h1>
        <Timeline />{*/ dialogContent}

      </div>
    </div>
    </div>);
  }
}
