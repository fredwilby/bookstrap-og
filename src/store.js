
import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

import reducer from './reducers/index';

const store = createStore(reducer, undefined,
  compose(applyMiddleware(promiseMiddleware()),
  window.devToolsExtension? window.devToolsExtension() : f=>f
  ));

export default store;
