
import axios from 'axios';
import keymirror from 'keymirror';

export const bookActions = keymirror({
  SEARCH:0,
  ADD_BOOK:0,
  FETCH_BOOKS:0,
  CLEAR_RESULT:0
});

export const fetchBooks = function() {
  return {
    type: bookActions.FETCH_BOOKS,
    payload: {
      promise: axios.get('/api/books')
    }
  };
}

export const titleSearch = function(title) {
  return {
    type: bookActions.SEARCH,
    payload: {
      promise: axios.get(`/api/books/search/${title}`)
    }
  };
}

export const addBook = function(asin) {
  return {
    type: bookActions.ADD_BOOK,
    payload:  {
      promise: axios.get(`/api/books/lookup/${asin}`)
    }
  };
}

export const clearResult = function() {
  return { type: bookActions.CLEAR_RESULT };
}
