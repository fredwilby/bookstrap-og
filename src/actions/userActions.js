import axios from 'axios';
import keymirror from 'keymirror';

export const userActions = keymirror({
  LOGIN:0,
  LOGOUT:0,
  SIGN_UP:0,
  GET_USER:0
});


export function getUser() {
  return {
    type: userActions.GET_USER,
    payload: {
      promise: axios.get('/auth/me')
    }
  };
}
