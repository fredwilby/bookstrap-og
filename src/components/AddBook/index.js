import React, {Component} from 'react';

import Modal from '../Modal';
import styles from './addbook.css';

import { connect } from 'react-redux';
import { titleSearch, addBook } from '../../actions/bookActions';

import AddIcon from '../../../static/icons/search.svg';
import CloseIcon from '../../../static/icons/plus.svg';

import BookTile from '../BookTile';

class AddBook extends Component {

  constructor(props) {
    super(props);

    this.state = { value: '' };
  }

  valueChange = (ev) => {
    this.setState({ value: ev.target.value });
  }

  titleSubmit = () => {
    this.props.titleSearch(this.state.value);
    this.setState({ value: '' });
  }

  selectBook(asin) {
    this.props.addBook(asin);
  }

  onKey = (ev) => {
    if(ev.which === 13 || ev.charCode === 13) {
      this.titleSubmit();
    }
  }

  render() {

    let results = null;

    if(this.props.loading) {

      results = <p className={styles.loading} >Loading...</p>;

    } else if(this.props.result && this.props.result.Items && (+this.props.result.Items.TotalResults) > 0) {
      results = this.props.result.Items.Item.map((el,ind)=> {

        let title = el.ItemAttributes.Title;
        let headline = '';

        if(title.indexOf(':') !== -1) {
          headline = title.split(':').slice(1).join(':').trim();
          title = title.split(':')[0];
        }

        return <BookTile className={styles.tile} key={ind} title={title}
                  author={el.ItemAttributes.Author} image={el.LargeImage.URL}
                  headline={headline} onClick={this.selectBook.bind(this, el.ASIN)} />
        });
    } else if (this.props.result && this.props.result.Items) {

      results = <p className={styles.loading}>No results</p>;

    } else if(this.props.result && this.props.result.asin) {
      this.props.onClose(this.props.result._id);
    }



    return (<Modal open={this.props.open} modalClass={styles.modal} overlayClass={styles.overlay} onClose={this.props.onClose} >
      <CloseIcon className={styles.close} onClick={this.props.onClose} />
      <h2 className={styles.title} >Add a book to this list</h2>

      <div className={styles.input}>
      <input type="text" value={this.state.value} onKeyDown={this.onKey} onChange={this.valueChange} placeholder="Search for books on Amazon..." />
      <a className={styles.button} onClick={this.titleSubmit} ><AddIcon /></a>
      </div>

      <div className={styles.results} >
      {results}
      </div>

    </Modal>);
  }


}

AddBook.propTypes = {
  open: React.PropTypes.bool.isRequired,
  onClose: React.PropTypes.func
};

const stateMap = (state, props) => ({
  result: state.get('books').get('amazonResult'),
  loading: state.get('books').get('amazonLoading')
});

export default connect(stateMap, { addBook, titleSearch })(AddBook);
