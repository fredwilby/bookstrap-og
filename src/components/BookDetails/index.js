import React, {Component} from 'react';
import styles from './bookdetails.css';

import AmazonLogo from '../../../static/icons/amazon-logo.svg';

import Modal from '../Modal';

const stripTags = (html) => {
  const div = document.createElement('div');
  div.innerHTML = html;

  return div.textContent;
}

class BookDetails extends Component {

  constructor(props) {
    super(props);

    this.state = { fullSummary: false };
  }

  toggleSummary = () => {
    this.setState({ fullSummary: !this.state.fullSummary });
  }

  render() {

    const { book, onClose } = this.props;

    let image, title, author, headline, summary, url;

    if(book) {
      image = book.get('image');
      title = book.get('title');
      author = book.get('author');
      headline = book.get('headline');
      summary = stripTags(book.get('summary'));

      if(summary === 'undefined') summary = undefined;

      url = book.get('url');
    }

    if(summary && !this.state.fullSummary) {
      summary = summary.substr(0, 205);
    }

    return (
      <Modal open={!!book} onClose={onClose} modalClass={styles.modal}>
        <img src={image} className={styles.cover} />
        <div className={styles.details} >
          <h2 className={styles.title}>{title}</h2>
          <h2 className={styles.author}>{author}</h2>
          <h2 className={styles.headline}>{headline}</h2>
          {
            summary &&
            <div><p className={styles.subtitle} >Summary</p>
            <p className={styles.summary} >{summary}</p>
            <p className={styles.expand} onClick={this.toggleSummary}><span>...</span></p>
            </div>
          }

          <a href={url} className={styles.amazon} ><AmazonLogo /></a>

        </div>
      </Modal>);
  }
}

BookDetails.propTypes = {
  book: React.PropTypes.object,
  onClose: React.PropTypes.func
};

export default BookDetails;
