import React, { Component } from 'react';

import styles from './modal.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import animationProps from './animation';

class Modal extends Component {

  constructor(props) {
    super(props);

    this.state = { hidden: !props.open };
  }

  componentWillReceiveProps(props) {

    if(!props.open && this.props.open) {
      setTimeout(()=>{ this.setState({hidden: true}); }, 300);
    }

    if(props.open && !this.props.open) {
      this.setState({ hidden: false });
    }

  }

  closeClick = (ev) => {
    if(ev.target.tagName.toLowerCase() === 'div' &&
      this.props.onClose &&
      (ev.target.className.indexOf(styles.overlay) !== -1 ||
      ev.target.parentNode.className.indexOf(styles.overlay) !== -1))
      this.props.onClose();
  }

  render() {

    const { hidden } = this.state;
    const { open, overlayClass, modalClass } = this.props;

    let overlay = styles.overlay +
      (hidden ? ` ${styles.hidden}` : '') +
      (overlayClass? ` ${overlayClass}` : '');


    return (<div className={overlay} onClick={this.closeClick}>
      <ReactCSSTransitionGroup {...animationProps} >
      {
        open &&
        (<div className={modalClass}>
          {this.props.children}
          </div>)
      }
      </ReactCSSTransitionGroup>
      </div>);
  }
}

Modal.propTypes = {
  open: React.PropTypes.bool.isRequired,
  overlayClass: React.PropTypes.string,
  modalClass: React.PropTypes.string,
  onClose: React.PropTypes.func
};

export default Modal;
