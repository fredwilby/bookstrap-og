import styles from './modal.css';

const animation = {
  transitionName: {
    enter: styles.enter,
    enterActive: styles.enterActive,
    leave: styles.leave,
    leaveActive: styles.leaveActive,
    appear: styles.enter,
    appearActive: styles.enterActive
  },

  transitionAppear: true,
  transitionEnterTimeout: 300,
  transitionLeaveTimeout: 300,
  transitionAppearTimeout: 300
};

export default animation;
