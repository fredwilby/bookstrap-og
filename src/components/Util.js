import React from 'react';

import logoStyles from '../styles/Logo.css';
import Book from '../../static/icons/logo.svg';

export function Logo(props) {

  let { className } = props;

  return (<div className={logoStyles.logo + " " + className}>
          <Book className={logoStyles.logoPic} />
          <h1 className={logoStyles.logoBig}><span className={logoStyles.logoBold}>Book</span>strap</h1>
          <h1 className={logoStyles.logoSmall}>.xyz</h1>
        </div>);
};

import loadMoreStyles from '../styles/LoadMore.css';
import WordTyper from './WordTyper';

export function LoadMore(props) {

  let text = "Load More...";

  if(props.isLoading)
    text = <WordTyper preEraseDelay={500} preWriteDelay={750} placeholder="..." words={['Loading...']} />;

  return (
    <div className={loadMoreStyles.buttonWrap}>
      <a className={loadMoreStyles.button} onClick={props.onClick} >{text}</a>
    </div>);
}
