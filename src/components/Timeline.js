import React from 'react';
import styles from '../styles/Timeline.css';

export default (props) => {

return (<div className={styles.timeline}>
  <div className={styles.bubbleWrap}>
    <ul className={styles.below}>
      <li>Customer Development</li>
      <li>Idea Validation</li>
      <li>Startup Life</li>
      <li>Minimum Viable Product</li>
    </ul>
    <div className={styles.bubble}>Idea</div></div>

  <div className={styles.bubbleWrap}>
    <ul className={styles.above}>
      <li>Beta Testing</li>
      <li>Building a Team</li>
      <li>Pivoting</li>
      <li>Execution</li>
    </ul>
    <div className={styles.bubble + " " + styles.empty}></div>
  </div>
  <div className={styles.bubbleWrap}>
    <ul className={styles.below}>
      <li>Investment</li>
      <li>Marketing</li>
      <li>Hiring Employees</li>
      <li>Finance</li>
    </ul>
    <div className={styles.bubble + " " + styles.empty}></div>
  </div>

  <div className={styles.bubbleWrap}>
    <ul className={styles.above}>
      <li>Company Culture</li>
      <li>Valuation</li>
      <li>IPO</li>
      <li>Angel Investing</li>
    </ul>
  <div className={styles.bubble}>Exit</div>
  </div>
</div>);

}
