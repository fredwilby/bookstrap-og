import React from 'react';
import logoStyles from '../../styles/Logo.css';
import styles from './header.css';

import { Link } from 'react-router';

import { Logo } from '../Util';

import DiscoverIcon from '../../../static/icons/discover.svg';
import BrowseIcon from '../../../static/icons/browse.svg';
import TopTagsIcon from '../../../static/icons/toptags.svg';

import SearchIcon from '../../../static/icons/search.svg';
import AddIcon from '../../../static/icons/plus.svg';

let Header = (props) => {

  let { onAddClick, onShareClick } = props;

  return (<div className={styles.header}>


    <Logo className={styles.logo} />

  <div className={styles.container}>
    <Link className={styles.pageLink} activeClassName={styles.active} to="/">
    <BrowseIcon className={styles.icon} />
    Browse</Link>
    <Link className={styles.pageLink} activeClassName={styles.active} to="/topcharts">
    <DiscoverIcon className={styles.icon} />
    Discover</Link>
    <Link className={styles.pageLink} activeClassName={styles.active} to="/tags">
    <TopTagsIcon className={styles.icon} />
    Top Tags</Link>
  </div>

  <div className={styles.rightContainer}>
  <a className={styles.circleButton} onClick={props.onSearchClick} ><SearchIcon /></a>
  <a className={styles.circleButton} onClick={props.onAddClick} ><AddIcon /></a>
  <a className={styles.profileButton} onClick={props.onProfileClick} ><img src={props.profilePic} /></a>
  </div>

  </div>);
};

Header.propTypes = {
  onAddClick: React.PropTypes.func,
  onSearchClick: React.PropTypes.func,
  onProfileClick: React.PropTypes.func,
  profilePic: React.PropTypes.string
};

export default Header;
