import React, {Component} from 'react';

let _timer = null;

class WordTyper extends Component {

  constructor(props) {
    super(props);

    this.state = { word:"", wInd:0 };
  }

  addLetter = () => {
    if(this.state.word.length < this.props.words[this.state.wInd].length)
    {
      this.setState({ word: this.state.word + this.props.words[this.state.wInd].charAt(this.state.word.length)});
      _timer = setTimeout(this.addLetter, this.props.typeDelay);
    } else {
      _timer = setTimeout(this.delLetter, this.props.preEraseDelay);
    }
  };

  delLetter = () => {
    if(this.state.word.length > 0)
    {
      this.setState({ word: this.state.word.slice(0,-1)});
      _timer = setTimeout(this.delLetter, this.props.typeDelay);
    } else {
      this.setState({ wInd: (this.state.wInd+1)%this.props.words.length });
      _timer = setTimeout(this.addLetter, this.props.preWriteDelay);
    }
  };

  componentDidMount() {
    this.addLetter();
  }

  componentWillUnmount() {
    clearTimeout(_timer);
  }

  render() {
    return <span className={this.props.className} >{this.state.word || this.props.placeholder }</span>;
  }
};

WordTyper.propTypes = {
  className:  React.PropTypes.string,
  words: React.PropTypes.array.isRequired,
  typeDelay: React.PropTypes.number,
  preEraseDelay: React.PropTypes.number,
  preWriteDelay: React.PropTypes.number,
  placeholder: React.PropTypes.string
};

WordTyper.defaultProps = {
  typeDelay: 75,
  preEraseDelay: 1500,
  preWriteDelay: 500,
  placeholder: "?"
};

export default WordTyper;
