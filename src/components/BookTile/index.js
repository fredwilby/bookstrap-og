import React from 'react';

import styles from './booktile.css';

let BookTile = (props) => {
  let { image, title, headline, author, topic, onClick, className } = props;

  const click = () => { onClick() };

  let tileStyle = styles.tile;

  if(className)
    tileStyle += ' ' + className;

  return (<div className={tileStyle} onClick={click}>
    <img className={styles.image} src={image} />
    <div className={styles.container}>
      <p className={styles.title}>{title} <span className={styles.author}>{author}</span></p>
      <p className={styles.headline}>{headline}</p>
    </div>
  </div>);

};

BookTile.propTypes = {
  image: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
  headline: React.PropTypes.string,
  author: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func,
  className: React.PropTypes.string
}

export default BookTile;
